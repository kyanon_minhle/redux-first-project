import React from 'react';
import './App.css';
// import Counter from '../components/Counter';
// import { Provider } from 'react-redux';
// import configureStore from '../store/configurationStore';

// import { useSelector, useDispatch } from 'react-redux';
// import { createStore, combineReducers } from 'redux';


import IndecisionApp from '../components/IndecisionApp'

function App() {
  return(
    <div className="App">
      <IndecisionApp />
    </div>
  )
}

export default App;
