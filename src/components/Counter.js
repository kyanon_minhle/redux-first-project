import React from 'react';
import { connect } from 'react-redux';

import { testLoad, incrementCounter, decrementCounter, resetCounter } from '../actions/counter';

function Counter(props) {
    return(
        <div>
            {props.counter}
            <button onClick={() => props.dispatch(incrementCounter())}>+</button>
            <button onClick={() => props.dispatch(decrementCounter())}>-</button>
            <button onClick={() => props.dispatch(resetCounter())}>reset</button>
            <input onChange={(e) => props.dispatch(testLoad({text : e.target.value}))}/>
            {props.data.text}
        </div>
    )
}


const mapStatetoProps = (state) => {
    console.log(state)
    return {
        counter : state.counter,
        data : state.testLoad.data
        
    }
};

export default connect(mapStatetoProps)(Counter);