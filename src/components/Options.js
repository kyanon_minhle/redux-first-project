import React from 'react';
import Option from './Option';

export default function Options({options, handleDeleteOption, handleDeleteAllOptions, hasOptions}) {
    return(
      <>
        <h3 className="widget-header__title">Your Options</h3>
        <button 
          onClick={handleDeleteAllOptions}
          className="button button--link" 
          disabled={!hasOptions}       
        >
          Remove All
        </button>
          { 
            options.length === 0 && <p className="widget__message"> Please add a option to get started!</p> 
          }
          {
              options.map((option,index) => <Option key={index} option={option} handleDeleteOption={handleDeleteOption}/>)
          }
      </>
    )
}