import React from 'react';

export default function AddOption({ addOption, error }) {
  // const [error, setError ] = useState(undefined);

  // const handleAddOption = (e) => {
	// 	e.preventDefault();
	// 	const option = e.target.elements.option.value.trim();
		
  //   setError(addOption(option));
	// 	if(!error){
	// 		e.target.elements.option.value = '';
	// 	}
	// }
    return(
      <>
        {
          error && <p className="add-option-error">{error}</p>
        }
        <form onSubmit={addOption}>
          <input 
              type='text'
              name="option"
          />
          <button>Add Option</button>
        </form>
      </>
    )
}