import React from 'react';

export default function Option({option, handleDeleteOption}) {
  return(
    <div>
      <p>{option}</p>
      <button 
          onClick={() => handleDeleteOption(option)}	
      >
          Remove Item
      </button>
    </div>
  )
}