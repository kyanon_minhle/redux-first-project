import React, { useState } from 'react';
import Header from './Header';
import Options from './Options'
import AddOption from './AddOption'
import Decision from './Decision'

export default function IndecisionApp(){
  const title = 'Indecision App';
  const subTitle = 'Put your life in hands of a computer';
  const [options, setOptions] = useState([])
  const [error, setError] = useState(undefined);
  const [selectedOption, setSelectedOption] = useState(undefined);

  //add a opton to state, store it at options = []
  const addOption = (e) => {
    e.preventDefault();
    const option = e.target.elements.option.value.trim();

    //check if the input value equals to empty string or a exist option then throw a error message. If everything is ok, change the state
		if(!option){
			setError('Please enter valid value to add option');
		} else if (options.indexOf(option) > -1) {
			setError('This option already exists')
    } else {
      const newOptions = [...options, option];
      setOptions(newOptions);
      setError(undefined);
    }
    //clear input value 
    if(!error){
      e.target.elements.option.value = '';
    }
  }

  //delete single option when cliking remove item
  const handleDeleteOption = (optionToRemove) => {
    const newOptions = options.filter((option) => optionToRemove !== option )
    setOptions(newOptions);
  }

  // delete All option when clicking remove all 
  const handleDeleteAllOptions = () => {
    setOptions([]);
    setError(undefined);// clear error if it exist 
  }

  //Pick a random option 
  const handlePick = () =>{
    const randomNum = Math.floor(Math.random() * options.length);
    setSelectedOption(options[randomNum]);
    alert(selectedOption);
  }
  return(
    <>
      <Header title={title} subtitle={subTitle} />
      <main>
        <Decision 
          hasOptions={options.length > 0}
          handlePick={handlePick}
        />
        <Options 
          options={options} 
          handleDeleteOption={handleDeleteOption} 
          handleDeleteAllOptions={handleDeleteAllOptions}
          hasOptions={options.length > 0}
        />
        <AddOption 
          addOption={addOption}
          error={error}
        />
      </main>
    </>
  )
}