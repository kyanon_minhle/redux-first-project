import React from 'react';

export default function Decison({ handlePick, hasOptions }) {
    return <button onClick={handlePick} disabled={!hasOptions}>What should I do?</button>
}