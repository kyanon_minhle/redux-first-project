import React from 'react';

export default function Header({title = 'Title', subtitle = 'Subtitle'} ) {
    return(
        <>
            <h1>{title}</h1>
            <p>{subtitle}</p>
        </>
    )
}
